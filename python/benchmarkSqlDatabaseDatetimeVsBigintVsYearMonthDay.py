# coding=utf-8
import time
from datetime import datetime, timedelta
from typing import List, Dict

from docker.models.networks import Network

import docker
import pprint

from sqlalchemy.orm import Query

from benchmarkDb.DockerDb import DockerDbConfig, DockerDb
from benchmarkDb.tables.timerange import TimeRange
from python.utils.logutils import status

pp = pprint.PrettyPrinter(indent=4)





mysql = DockerDbConfig(
    container="mysql",
    version="8.0.11",
    # version="5.7.22",
    port=8306
)
mariadb = DockerDbConfig(
    container="mariadb",
    version="10.3.8-bionic",
    port=8307
)

client: docker.DockerClient = docker.from_env()

container_mariadb = DockerDb(mariadb, client)
container_mysql = DockerDb(mysql, client)



network: Network = client.networks.create(
    name=mariadb.network
)


dbs: List[DockerDb] = [container_mariadb, container_mysql]
def prepare_select_time_range(
        days: int = 5,
        stepsize: int = 2,
        switch_db_after_day: bool = True):
    testname = "time_range"
    from benchmarkDb.tables import timerange
    for db in dbs:
        db.prepare_volume(testname)
        db.run_new_container()
    for db in dbs:
        db.wait()
        db.prepare_database()
        timerange.insert(db.engine)

    iter: List[List[DockerDb]] = list()
    if switch_db_after_day:
        iter = [dbs]
    else:
        iter = [[db] for db in dbs]

    for singleInter in iter:
        # 1 day = 24 hours
        # 24 hours = 1.440 Minutes
        # 1.440 Minutes = 86.400 Seconds
        # 86.400 Seconds = 86.400.000 Miliseconds
        startdate: datetime = datetime(2017, 1, 1, 0, 0, 0)
        status("Starting to create %s days of data in ms with step size %s" % (days, stepsize))
        for day in range(0, days):
            for hour in range(0, 24):
                # 3.600.000 Entries will be inserted here.
                testdata: List[TimeRange] = []
                for minute in range(0, 60):
                    for second in range(0, 60):
                        for millisecond in range(0, 1000, stepsize):
                            testdata.append(TimeRange(timestamp=startdate - timedelta(
                                days=day,
                                hours=hour,
                                minutes=minute,
                                seconds=second,
                                milliseconds=millisecond
                            )))
                status("Saving day %s, hour %s" % (day, hour))
                for db in singleInter:
                    db.insert_and_commit(testdata)
                status("...DONE")
        for db in singleInter:
            db.disconnect_db()
            db.stop_container()
    return


def execute_select_time_range(loops: int = 2) -> Dict[str, Dict[str, List[float]]]:
    dateStart: datetime = datetime(2016, 12, 31, 0, 0, 0)
    dateEnd: datetime = datetime(2016, 12, 31, 1, 0, 0)

    timestampEnd: int = int(dateEnd.timestamp() * 1000)
    timestampStart: int = int(dateStart.timestamp() * 1000)

    timing_per_db: Dict[str, Dict[str, List[float]]] = dict()
    for db in dbs:
        # deactivating query cache

        queryTimestamp: Query =  db.session.query(TimeRange)\
            .filter(TimeRange.timestamp >= timestampStart,
                    TimeRange.timestamp < timestampEnd)
        queryDate: Query =  db.session.query(TimeRange)\
            .filter(TimeRange.date >= dateStart,
                    TimeRange.date < dateEnd)
        queryIndex: Query = db.session.query(TimeRange)\
            .filter(TimeRange.year == dateStart.year,
                    TimeRange.month == dateStart.month,
                    TimeRange.day == dateStart.day,
                    TimeRange.hour == dateStart.hour)


        timings: Dict[str, List[float]] = dict()
        timing_per_db[db.config.name] = timings
        queries: Dict[str, Query] = {"query_bigint_range":queryTimestamp,
                                     "query_datetime":queryDate,
                                     "query_idpairs":queryIndex}

        for key in queries:
            query: Query = queries[key]
            singleTiming: List[float] = []
            timings[key] = singleTiming
            print(" ... Testing %s with db %s" % (key, db.config.name))
            print(" ... Query: ")
            print(str(query))
            for i in range(loops):
                t1s = time.time()
                _ = query.count()
                t1e = time.time()
                t1d = t1e - t1s
                singleTiming.append(t1d)
                print(" --- Time needed: %s" % str(t1d))
    return timing_per_db

def start_dbs():
    for db in dbs:
        db.start_container()
        db.wait()
        db.prepare_database()
        # db.connect_db()
    container_mariadb.engine.execute("SET SESSION query_cache_type = OFF;")
    return


def close_dbs():
    for db in dbs:
        try:
            db.disconnect_db()
        except:
            pass
        db.stop_container()
    return


# input("Please press enter to shutdown:")
# session.close()
# container_phpmyadmin.stop()
# container_sql.stop()
# network.remove()



# Testing query_bigint_range
# Query:
# SELECT test01.id AS test01_id, test01.timestamp AS test01_timestamp, test01.year AS test01_year, test01.month AS test01_month, test01.day AS test01_day, test01.hour AS test01_hour, test01.date AS test01_date
# FROM test01
# WHERE test01.timestamp >= %(timestamp_1)s AND test01.timestamp < %(timestamp_2)s
#  --- Time needed: 9.215585708618164
#  --- Time needed: 9.023510217666626
# Testing query_datetime
# Query:
# SELECT test01.id AS test01_id, test01.timestamp AS test01_timestamp, test01.year AS test01_year, test01.month AS test01_month, test01.day AS test01_day, test01.hour AS test01_hour, test01.date AS test01_date
# FROM test01
# WHERE test01.date >= %(date_1)s AND test01.date < %(date_2)s
#  --- Time needed: 12.64231276512146
#  --- Time needed: 12.566076755523682
# Testing query_idpairs
# Query:
# SELECT test01.id AS test01_id, test01.timestamp AS test01_timestamp, test01.year AS test01_year, test01.month AS test01_month, test01.day AS test01_day, test01.hour AS test01_hour, test01.date AS test01_date
# FROM test01
# WHERE test01.year = %(year_1)s AND test01.month = %(month_1)s AND test01.day = %(day_1)s
#  --- Time needed: 9.335744619369507
#  --- Time needed: 8.852861404418945

