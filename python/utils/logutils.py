# coding=utf-8
from datetime import datetime
from ansimarkup import ansiprint as print

def status(message: str):
    print('<green><b> ...</b> %s at</green> <blue>%s</blue>' % (message, f'{datetime.now():%Y-%m-%d %H:%M:%S.%f}'))
