# coding=utf-8

def ask_user(message: str) -> bool:
    msg: str = message + " (Y|n)"
    while True:
        raw_input = input(msg)
        raw_input = raw_input.lower()
        if raw_input == "" or raw_input == "y":
            return True
        elif raw_input == "n":
            return False
        else:
            print("ERROR: Please enter 'y' for yes or 'n' for no")
