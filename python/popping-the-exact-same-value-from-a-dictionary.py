# coding=utf-8

llist = ["a", "aa", "ab", "c"]
dict1 = {"xax": 1, "xaax": 2, "xabx": 3, "xabcx": 4}
llist.sort(key=len, reverse=True)
dict2 = dict()
for key in dict1:
    success = False
    for replace_token in llist:
        if replace_token in key:
            dict2[replace_token] = dict1.get(key)
            success = True
            break
    if not success:
        dict2[key] = dict1.get(key)

print(dict2)


from typing import List
llist = ["a", "aa", "ab", "c"]
dict1 = {"xax": 1, "xaax": 2, "xabx": 3, "xabcx": 4}
llist.sort(key=len, reverse=True)
dict2 = dict()
for key in dict1:
    success = False
    for replace_token in llist:
        if replace_token in key:
            current = dict2.get(replace_token, [])
            new = dict1.get(key)
            if isinstance(new, List):
                current.extend(new)
            else:
                current.append(new)
            dict2[replace_token] = current
            success = True
            break
    if not success:
        dict2[key] = dict1.get(key)
print(dict2)
