# coding=utf-8
from typing import Optional

from docker import DockerClient
from docker.models.containers import Container
from docker.models.volumes import Volume
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.ext.declarative import DeclarativeMeta
from sqlalchemy.orm import sessionmaker, Session

from python.utils.waitForMysql import wait_for_database


class DockerDbConfig:
    host: str
    port: int
    # username: str
    password: str
    db: str
    driver: str
    network: str
    volume: str
    name: str
    container: str
    version: str
    mount_point: str

    def __init__(
            self,
            container: str,
            version: str,
            volume: Optional[str] = None,
            name: Optional[str] = None,
            network: str = "python_benchmark_db",
            port: int = 3306,
            host: str = "127.0.0.1",
            # username: str = "root",
            password: str = "admin123",
            db: str = "test01",
            driver: str = "mysql+pymysql",
            mount_point: str = "/var/lib/mysql"
    ):
        self.host = host
        self.port = port
        # self.username = username
        self.password = password
        self.db = db
        self.driver = driver
        self.network = network
        self.volume = volume
        self.container = container
        self.version = version
        self.volume = volume or "python_benchmark_db_%s" % container
        self.name = name or "python_benchmark_db_%s" % container
        self.mount_point = mount_point

class DockerDb:
    volume: Optional[Volume]
    container: Optional[Container]
    config: DockerDbConfig
    client: DockerClient
    engine: Engine
    current_test: str = "" # TODO: refactor!!!
    session_builder: sessionmaker
    session: Session

    def __init__(self, config: DockerDbConfig, client: DockerClient):
        self.config = config
        self.client = client
        return

    def prepare_volume(self, testname: str):
        self.current_test = testname
        vol: str = "_".join([self.config.volume, self.current_test])
        if vol in [elem.name for elem in self.client.volumes.list()]:
            self.volume = self.client.volumes.get(vol)
        else:
            self.volume = self.client.volumes.create(self.config.volume)
        return

    def run_new_container(self):
        con: str = "_".join([self.config.container, self.current_test])
        if con in [elem.name for elem in self.client.containers.list(all=True)]:
            to_del: Container = self.client.containers.get(con)
            try:
                to_del.stop()
                to_del.remove()
            except:
                pass
        self.container = self.client.containers.run(
            image="%s:%s" % (self.config.container, self.config.version),
            auto_remove=False,
            remove=False,
            detach=True,
            ports={"3306/tcp":("127.0.0.1", self.config.port)},
            environment={"MYSQL_ROOT_PASSWORD":self.config.password,
                         "MYSQL_DATABASE": self.config.db},
            name=self.config.name,
            network=self.config.network,
            volumes={self.config.volume: {'bind': self.config.mount_point, 'mode': 'rw'}}
        )
        return

    def start_container(self):
        self.container.start()
        return

    def stop_container(self):
        self.container.stop()
        return

    def wait(self):
        wait_for_database(
            host=self.config.host,
            port=self.config.port,
            db=self.config.db,
            user="root",
            password=self.config.password,
            checking_interval_seconds=1
        )
        return

    def prepare_database(self):
        self.engine = create_engine('{mysql_driver}://{user}:{password}@{host}:{port}/{database}'.format(**{
            "mysql_driver": self.config.driver,
            "user":         "root",
            "password":     self.config.password,
            "host":         self.config.host,
            "port":         self.config.port,
            "database":     self.config.db
        }), pool_size=5, max_overflows=0)

        self.session_builder = sessionmaker()
        self.session_builder.configure(bind=self.engine)
        self.session: Session = self.session_builder()
        return

    def insert_and_commit(self, testdata):
        self.session.bulk_save_objects(testdata)
        self.session.commit()
        return

    def disconnect_db(self):
        self.session.close()
        self.session = None
        return

    def connect_db(self):
        self.session_builder = sessionmaker()
        self.session_builder.configure(bind=self.engine)
        self.session = self.session_builder()
        return
