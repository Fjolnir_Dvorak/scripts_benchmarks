# coding=utf-8
from datetime import datetime

from requests import Session
from sqlalchemy import Column, Index
from sqlalchemy.dialects.mysql import BIGINT, SMALLINT, TINYINT, DATETIME
from sqlalchemy.engine import Engine
from sqlalchemy.ext.declarative import DeclarativeMeta, declarative_base


Base: DeclarativeMeta = declarative_base()
class TimeRange(Base):
    __tablename__ = "test01"

    id = Column(BIGINT(unsigned=True), primary_key=True, autoincrement=True, nullable=False, name="id")
    timestamp = Column(BIGINT(unsigned=True), nullable=False, index=True, name="timestamp")
    year = Column(SMALLINT(unsigned=True), nullable=False, index=False, name="year")
    month = Column(TINYINT(unsigned=True), nullable=False, index=False, name="month")
    day = Column(TINYINT(unsigned=True), nullable=False, index=False, name="day")
    hour = Column(TINYINT(unsigned=True), nullable=False, index=False, name="hour")
    date = Column(DATETIME(), nullable=False, index=True, name="date")

    def __init__(self, timestamp: datetime):
        self.date = timestamp,
        self.timestamp = round(timestamp.timestamp() * 1000) # millisecond percision
        self.year = timestamp.year
        self.month = timestamp.month
        self.day = timestamp.day
        self.hour = timestamp.hour

index = Index('user_index', TimeRange.year, TimeRange.month, TimeRange.day, TimeRange.hour)

def insert(engine: Engine):
    Base.metadata.create_all(engine)
